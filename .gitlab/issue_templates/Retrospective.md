<!--
  Replace {team} {discipline} with your team name and discipline; e.g. Plan
  backend, throughout or delete where appropriate.
-->

<!--
 Please title this issue in the following format:

   {team} {discipline} Engineering Internship Retrospective
-->

This is an asynchronous retrospective for the [engineering internship
pilot](https://about.gitlab.com/handbook/engineering/internships/). It's
private to the {team} team, the intern, plus anyone else that worked with the team during
internship.

This retrospective will follow the process described at
https://about.gitlab.com/handbook/engineering/management/team-retrospectives/.
Please feel free to honestly describe your thoughts and feelings about the internship pilot program below.

Please look back at your experiences hosting (or participating as) an intern, ask yourself
**👍 what went well?**, **👎 what didn’t go well?**, and **📈 what could be
improved in the next iteration of the programme?**, and honestly describe your
thoughts and feelings below.

For each point you want to raise, please create a new discussion with the
relevant emoji, so that others can weigh in with their perspectives, and so that
we can easily discuss any follow-up action items in-line.

(If there is something you are not comfortable sharing, message your manager
directly. But note that 'Emotions are not only allowed in retrospectives, they
should be encouraged', so we'd love to hear from you here if possible.)

This issue will be made public when the retrospective is complete.

/confidential
/label retrospective

<!--
  Please assign to all team members, especially mentors
  /assign
-->

<!-- TODO: auto-subscribe pilot programme admins -->
<!-- TODO: auto-assign to chairperson -->
<!-- TODO: set common due date -->
